<?php

return array (
  'autoload' => false,
  'hooks' => 
  array (
    'content_delete_end' => 
    array (
      0 => '\\app\\member\\behavior\\Hooks',
    ),
    'content_edit_end' => 
    array (
      0 => '\\app\\member\\behavior\\Hooks',
    ),
    'user_sidenav_after' => 
    array (
      0 => '\\app\\pay\\behavior\\Hooks',
      1 => '\\app\\message\\behavior\\Hooks',
      2 => '\\app\\cms\\behavior\\Hooks',
    ),
    'app_init' => 
    array (
      0 => '\\app\\cms\\behavior\\Hooks',
    ),
  ),
  'route' => 
  array (
  ),
);