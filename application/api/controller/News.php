<?php

namespace app\api\controller;

use app\api\model\ConfigModel;
use app\api\model\MemberModel;
use app\api\model\NewsTimeManageModel;
use app\api\model\NewsTopTypeModel;
use app\api\model\ProTopTypeConditionModel;
use app\api\model\ProTopTypeModel;
use app\api\model\TypeTmpRankingModel;
use app\common\controller\Api;

/**
 * @title 文章
 * @controller api\controller\News
 * @group base
 */
class News extends Api
{

    /**
     * @title 获取排行分类
     * @url /api/News/getNewsRankingType
     * @method GET
     * @param name:token type:string require:1 desc:token
     * @return name:data type:array ref:definitions\dictionary
     */
    public function getNewsRankingType(){

        $proTopTypeModel = new ProTopTypeModel();

        $news = $proTopTypeModel->getTopType();

        $this->success("获取成功",$news);
    }

    /**
     * @title 发帖条件
     * @url /api/News/postCondition
     * @method GET
     * @return name:data type:array ref:definitions\dictionary
     */
    public function postCondition(){

        $typeid = input("get.typeid");
        $timeManageModel = new NewsTimeManageModel();
        $timeManageInfo = $timeManageModel->getTimeManage($typeid);
        $this->success("获取成功",$timeManageInfo);
    }

    /**
     * @title 获取分成百分比
     * @url /api/News/getdividedInfo
     * @method GET
     * @return name:data type:array ref:definitions\dictionary
     */
    public function getdividedInfo(){

        $configModel = new ConfigModel();

        $dividedInfo = $configModel->getdividedInfo();

        $this->success("获取成功",$dividedInfo);
    }

    /**
     * @title 排行计算
     * @url /api/News/ranking
     * @method GET
     * @return name:data type:array ref:definitions\dictionary
     */
    public function ranking(){

        $proTopTypeModel = new ProTopTypeModel();
        $proTopTypeConditionModel = new ProTopTypeConditionModel();
        $memberModel = new MemberModel();
        $newsTopTypeModel = new NewsTopTypeModel();
        $TypeTmpRankingModel = new TypeTmpRankingModel();

        //查找专家用户
        $proMemberInfos = $memberModel->findProMemberInfo();

        //增加专家分类查询

        //查找专家分类下的排行分类
        $type = $proTopTypeModel->getTopType();

        $memberNewsTypeArray = array();
        foreach($proMemberInfos as $proMemberInfoKey => $proMembervalue){
            foreach($type as $typevalue){
                $errorNum = 0;
                $rightNum = 0;
                $istop = 0;
                //文章总数
                $total = $newsTopTypeModel->getNewsNumByRankingId($proMembervalue['userid'],$typevalue['id']);
                //文章信息
                $tmpInfos = $newsTopTypeModel->getNewsIdByRankingId($proMembervalue['userid'],$typevalue['id']);
                if($tmpInfos!=null&&count($tmpInfos)>0){
                    $memberNewsTypeArray['memberid'] = $proMembervalue['userid'];
                    $memberNewsTypeArray['rankingtype'] = $typevalue['id'];

                    $conditionInfos = $proTopTypeConditionModel->getConditionByTypeId($typevalue['id']);
                    foreach($conditionInfos as $conditionInfosValue) {
                        $countArray = array();
                        foreach ($tmpInfos as $tmpInfoValue) {
                            //拥有用户id及排名类型
                            //判断当前错误数是否大于条件错误数
                            if($tmpInfoValue['toptypeid'] == $typevalue['id']) {
                                if ($errorNum <= $conditionInfosValue['errornum'] + 1) {
                                    //判断当前文章记录是否正确
                                    if ($tmpInfoValue['is_right'] == -1) {
                                        //插入连对数
                                        array_push($countArray, $rightNum);
                                        //增加错误数
                                        $errorNum++;
                                        array_push($countArray, -1);
                                        $rightNum = 0;
                                    } else {
                                        //增加正确数
                                        $rightNum++;
                                    }
                                }
                            }
                        }
                        if($rightNum>0){
                            array_push($countArray,$rightNum);
                        }
                        //计算百分比
                        $percentage = (max($countArray)/$total)*100;
                        $result = $percentage-$conditionInfosValue['topright'];
                        if($result>0 && $result>$istop){
                            $istop = $conditionInfosValue['istop'];
                        }
                        $rightNum = 0;
                    }
                    $memberNewsTypeArray['rightnum'] = max($countArray);
                    $memberNewsTypeArray['total'] = $total;
                    $memberNewsTypeArray['istop'] = $istop;
                    $memberNewsTypeArray['createtime'] = time();
                    $TypeTmpRankingModel->saveTypeTmpRanking($memberNewsTypeArray);
                }
            }
        }
        $this->success("获取成功");
    }

}