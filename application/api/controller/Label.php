<?php

namespace app\api\controller;

use app\api\model\LabelModel;
use app\common\controller\Api;
use think\Exception;

/**
 * @title 栏目
 * @controller api\controller\label
 * @group base
 */
class Label extends Api
{

    /**
     * @title  获取栏目
     * @url /api/label/getlabel
     * @method GET
     * @return name:data type:array ref:definitions\dictionary
     */
    public function getlabel(){
        try {
            $labelModel = new LabelModel();
            $clazz = $labelModel->getLabel();
            foreach($clazz as $key=>$value){
              $subclazz = $labelModel->getSubLabel($value['classid']);
              $clazz[$key]['subclazz'] = $subclazz;
            }

            return $clazz;
        }catch (Exception $e){
            $this->error("接口异常");
        }
    }



}