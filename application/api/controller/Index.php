<?php
namespace app\api\controller;

use app\api\model\LabelModel;
use app\api\model\MemberInterestClazzModel;
use app\api\model\MemberModel;
use app\api\model\NewsModel;
use app\common\controller\Api;
use think\Exception;

/**
 * @title 首页
 * @controller api\controller\Index
 * @group base
 */
class Index extends Api
{
    /**
     * @title 首页
     * @url /api/Index/index
     * @method GET
     * @return name:data type:array ref:definitions\dictionary
     */
    public function index()
    {
        try {
            $result = [];
            //栏目数据
            $labelModel = new LabelModel();
            $clazz = $labelModel->getLabel();
            foreach($clazz as $key=>$value){
                $subclazz = $labelModel->getSubLabel($value['classid']);
                $clazz[$key]['subclazz'] = $subclazz;
            }
            $result['clazz'] = $clazz;

            $this->success('发送成功',$clazz);
        }catch (Exception $e){
            $this->error("接口异常");
        }
    }

    /**
     * @title 获取指定标签下的文章
     * @url /api/index/getlabelnews
     * @param name:token type:string desc:token
     *
     * @param name：page type:int desc:页数默认1
     * @param name:size type:int desc:每页条数默认10
     * @param name:classid type:int require:1 desc:标签id
     * @method GET
     * @return name:data type:array ref:definitions\dictionary
     */
    public function getlabelnews(){
        try {
            $data = input("post.");
            $newsModel = new NewsModel();
            if($data['token']){
                //获取用户兴趣标签
                $memberModel = new MemberModel();
                $member = $memberModel->findMemberByToken($data['token']);
                $interestModel = new MemberInterestClazzModel();
                $interest = $interestModel->getMemberInterestClazz();
                //按用户兴趣标签获取文章
                $news = $newsModel->BatchgetNews($interest['clazzids']);

            }else{
                //默认获取文章
                $news = $newsModel->getNewsByLabel($data['classid']);
            }
            $this->success("获取成功",$news);
        }catch (Exception $e){
            $this->error("接口异常");
        }
    }

}
