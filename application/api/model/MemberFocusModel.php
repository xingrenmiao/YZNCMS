<?php

namespace app\api\model;

use think\Model;

class MemberFocusModel extends Model
{

    protected $table = "phome_enewsfile_member_focus";

    /**
     * 保存关注信息
     */
    public function insertfocus($userid,$focus_member_id){

        $insertInfo = [
            'userid'=>$userid,
            'focus_member_id'=>$focus_member_id,
            'createtime'=>time(),
        ];

        $flag = $this->replace()->insert($insertInfo);
        if($flag>0){
            return true;
        }else{
            return false;
        }
    }

}