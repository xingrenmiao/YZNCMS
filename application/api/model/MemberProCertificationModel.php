<?php

namespace app\api\model;

use think\Model;

class MemberProCertificationModel extends Model
{

    protected $table = "phome_member_pro_certification";


    /**
     * 申请专家认证
     */
    public function applicationProCertification($userid,$remark){

        $insertInfo = [
            'userid'=>$userid,
            'pro_certification'=>0,
            'pro_certification_remark'=>$remark,
            'createtime'=>time(),
        ];
        $this->insert($insertInfo);
    }

}