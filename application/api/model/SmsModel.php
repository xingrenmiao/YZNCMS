<?php

namespace app\api\model;

use think\Model;

class SmsModel extends Model
{
    protected $table = 'phome_sms';

    /**
     * 获得验证码
     * @param $mobile
     */
    public function getSmsByMobile($mobile){
        $sms = $this->where("mobile = {$mobile} ")->order("id","desc")->find();
        return $sms;
    }

    /**
     * 保存验证码使用次数
     * @param $code
     */
    public function saveCodeTimes($code){

        $sms = $this->where("code = {$code}")->find();

        if($sms!=null){
            $sms['times'] = $sms['times']+1;

//        $this->save($sms,"id = {$sms['id']}");

            $sql = "update phome_sms set times = times+1 where id = {$sms['id']}";

            $this->execute($sql);
            return true;
        }else{
            return false;
        }
    }



}