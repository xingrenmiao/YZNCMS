<?php

namespace app\api\model;

use think\Model;

class NewsModel extends Model
{

    protected $table = "phome_ecms_news";

    /**
     * 获取栏目文章
     */
    public function getNewsByLabel($classId,$page=1,$size=10){
        if(!empty($classId)){
            $labelNews =  $this->where("classid = {$classId}")->order("newtime","desc")->limit("{$page}","{$size}")->select();
        }else{
            $labelNews =  $this->order("newtime","desc")->limit("{$page}","{$size}")->select();
        }
        return $labelNews;
    }

    /**
     * 批量获取
     */
    public function BatchgetNews($clazzIds,$page=1,$size=10){

       $news = $this->where("classid in ({$clazzIds})")->order("newstime","desc")->limit("{$page}","{$size}")->select();

       return $news;
    }

    /**
     * 获取会员发布的文章
     */
    public function getMemberNews($userid,$page=1,$size=10){

        $news = $this->where("userid = {$userid}")->order("newstime","desc")->limit("{$page}","{$size}")->select();

        return $news;

    }

    /**
     * 保存文章信息
     */
    public function saveNews($data){
        $url = "127.0.0.1/api.php/News/createNews";
        //组装数据
        $data['checked'] = 0;
        $data['newstime'] = dateFormatSecond(time());
        $data['befrom_id'] = "选择信息来源";
        $data['autosize'] = "5000";
        $data['getfirsttitlespicw'] = "105";
        $data['getfirsttitlespich'] = "118";
        $data['newspath'] = dateFormat(time());
        $data['enews'] = "AddNews";

        $data = json_encode($data);
        $result = apiaccess($url,$data,1);

        return $result;
    }

    /**
     * 查找专家文章
     */
    public function findNewsByIspro(){

        $news = $this->where("ispro = 1  ")->select();

        return $news;
    }

}