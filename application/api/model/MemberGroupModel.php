<?php

namespace app\api\model;

use think\Model;

class MemberGroupModel extends Model
{

    protected $table = "phome_enewsmembergroup";

    /**
     * 获取用户组
     */
    public function getGroup($groupid){

        $group = $this->where("id = {$groupid}")->find();

        return $group;
    }

}