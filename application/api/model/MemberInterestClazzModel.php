<?php

namespace app\api\model;

use think\Model;

class MemberInterestClazzModel extends Model
{

    protected $table = "phome_member_interest_clazz";


    /**
     * 获取用户兴趣标签组
     */
    public function getMemberInterestClazz($userid){

        $interest = $this->where("userid = {$userid}")->find();

        return $interest;

    }

    /**
     * 插入用户标签组
     */
    public function insertMemberInterestClazz($userid,$clazzids){

        $interestClazzInfo = $this->where("userid = {$userid}")->find();
        if($interestClazzInfo!=null){
            $insertData = [
                "userid"=>$userid,
                "clazzids"=>$clazzids,
            ];

            $this->update($insertData,"userid = {$userid}");
        }else{
            $insertData = [
                "userid"=>$userid,
                "clazzids"=>$clazzids,
            ];

            $this->insert($insertData);
        }
    }

    /**
     * 是否选择兴趣
     */
    public function isMemberInterestClazz($userid){

       $interest = $this->where("userid = {$userid}")->find();

       if(!empty($interest)){
           return true;
       }else{
           return false;
       }
    }

}