<?php

namespace app\api\model;

use think\Model;

class ConfigModel extends Model
{

    protected $table="phome_config";

    /**
     * 获取分成百分比
     */
    public function getdividedInfo(){

       $dividedInfo = $this->where("name='dividedConfig' ")->find();

       return $dividedInfo;

    }

}