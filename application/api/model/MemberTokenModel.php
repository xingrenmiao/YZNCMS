<?php

namespace app\api\model;

use think\Model;
use util\Random;

class MemberTokenModel extends Model
{

    protected $table = "phome_enewsfile_member_token";

    private $keep_time = 86400;

    /**
     * 检查token是否过期
     * @param $token
     */
    public function getTokenInfo($token){

        $tokenInfo = $this->where("token = {$token}")->find();

        return $tokenInfo;

    }

    /**
     * 保存token
     */
    public function saveToken($memberId,$token){

        $tokenInfo = $this->where(" userid = {$memberId} ")->find();

        if($tokenInfo!=null){
            $update = [
              'token' => $token,
              'expiretime'  => time()+$this->keep_time,
              'ceartetime'  => time()
            ];
//            $tokenInfo['token'] = $token;
//            $tokenInfo['expiretime'] = time()+$this->keep_time;
//            $tokenInfo['ceartetime'] = time();
            $this->update($update,"userid = {$memberId}");
        }else{
            $tokenInfo['token'] = $token;
            $tokenInfo['userid'] = $memberId;
            $tokenInfo['expiretime'] = time()+$this->keep_time;
            $tokenInfo['ceartetime'] = time();
            $this->save($tokenInfo);
        }
    }

}