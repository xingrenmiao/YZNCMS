<?php

namespace app\api\model;

use think\Model;

class ProTopTypeConditionModel extends Model
{

    protected $table = "phome_pro_top_type_condition";

    /**
     * 获取指定类型的条件
     */
    public function getConditionByTypeId($typeId){

        $conditionInfos = $this->where("typeid = {$typeId}")->select();

        return $conditionInfos;
    }

}