<?php

namespace app\api\model;

use think\Model;

class LabelModel extends Model
{

    protected $table = "phome_enewsclass";

    /**
     * 获取主栏目标签
     */
    public function getLabel(){
        $clazz = $this->where("bclassid = 0")->select();

        return $clazz;
    }

    /**
     * 获取子菜单
     */
    public function getSubLabel($clazzid){

        $subclazz = $this->where("bclassid = {$clazzid}")->select();

        return $subclazz;
    }

}