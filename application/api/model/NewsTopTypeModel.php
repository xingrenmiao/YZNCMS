<?php

namespace app\api\model;

use think\Model;

class NewsTopTypeModel extends Model
{

    protected $table = "phome_ecms_news_top_type";


    /**
     * 按排名类型查找文章id
     * @param $rankingId
     */
    public function getNewsIdByRankingId($memberId,$rankingId){

       $newsTopTypeInfo = $this->where("memberId = {$memberId} and toptypeid = {$rankingId} and (is_right = -1 or is_right =1)")->select();

       return $newsTopTypeInfo;
    }

    /**
     * 获取该类型文章总数
     * @param $memberId
     * @param $rankingId
     */
    public function getNewsNumByRankingId($memberId,$rankingId){

        $newsTopTypeInfoNum = $this->where("memberId = {$memberId} and toptypeid = {$rankingId} and (is_right = -1 or is_right =1)")->count();

        return $newsTopTypeInfoNum;

    }


}