<?php

namespace app\api\model;

use think\Model;

class NewsTimeManageModel extends Model
{

    protected $table="phome_ecms_news_time_manage";

    /**
     * 获取发帖条件
     */
    public function getTimeManage($typeid){

        $timeManage = $this->where("typeid = {$typeid}")->find();

        return $timeManage;
    }

}