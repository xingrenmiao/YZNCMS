<?php

namespace app\api\model;

use think\Model;

class TypeTmpRankingModel extends Model
{

    protected $table = "phome_pro_top_type_tmp_ranking";


    /**
     * 获取排名
     */
    public function getTypeTmpRanking(){

        $ranking = $this->select();

        return $ranking;
    }

    /**
     * 保存临时排名
     */
    public function saveTypeTmpRanking($ranking){
        $this->insert($ranking);
    }

    /**
     * 删除老数据
     * @return bool|void
     */
    public function delete(){
        $this->delete();
    }

}