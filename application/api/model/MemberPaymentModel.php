<?php

namespace app\api\model;

use think\Exception;
use think\Model;

class MemberPaymentModel extends Model
{

    protected $table = "phome_member_payment";

    protected $pk = "userid";

    /**
     * 增加用户付款信息
     */
    public function saveMemberPayment($data){
        try {
            $data['createtime']=time();

            $this->save($data);
            return true;
        }catch (Exception $e){
            return false;
        }
    }

}