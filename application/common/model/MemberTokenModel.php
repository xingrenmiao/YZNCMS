<?php

namespace app\common\model;

use think\Model;

class MemberTokenModel extends Model
{

    protected $table = "phome_enewsfile_member_token";

    private $keepTime = 86400;

    /**
     * 检查token是否过期
     * @param $token
     */
    public function getTokenInfo($token){

        $tokenInfo = $this->where("token = '{$token}'")->find();

        return $tokenInfo;

    }

    /**
     * 刷新token时间
     */
    public function flashTokenTime($tokenInfo){
        $tokenInfo['expire_time'] = time()+$this->keepTime;
        $this->save($tokenInfo,"id = {$tokenInfo['id']}");

    }

}